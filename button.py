import pygame
from utils import *

class Arrow:
    arrows = []

    def __init__(self, x, y, width, height, color, function, direction):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        if direction == 'right':
            self.rect = pygame.Rect(x, y, width, height)
        if direction == 'left':
            self.rect = pygame.Rect(x-width, y, width, height)
        self.color = color
        self.function = function
        self.direction = direction
        Arrow.arrows.append(self)

    def draw(self, win):
        if self.direction == 'right':
            pygame.draw.polygon(win, self.color, ((self.x, self.y), (self.x+self.width, self.y+self.height//2), (self.x, self.y+self.height),  (self.x, self.y+self.height-15), (self.x+self.width-self.width//3, self.y+self.height//2), (self.x, self.y+15)))
        if self.direction == 'left':
            pygame.draw.polygon(win, self.color, ((self.x, self.y), (self.x-self.width, self.y+self.height//2), (self.x, self.y+self.height),  (self.x, self.y+self.height-15), (self.x-self.width+self.width//3, self.y+self.height//2), (self.x, self.y+15)))

    def go(self):
        while len(Button.buttons) > 0:
            for button in Button.buttons:
                Button.buttons.remove(button)
                del button
        while len(Arrow.arrows) > 0:
            for arrow in Arrow.arrows:
                Arrow.arrows.remove(arrow)
                del arrow
        self.function()


class Button:
    buttons = []

    def __init__(self, x, y, width, height, color, text, text_color, text_size, action, level, msg1, msg2):
        self.rect = pygame.Rect(x, y, width, height)
        self.action = action
        self.text_color = text_color
        self.level = level
        self.msg1 = msg1
        self.msg2 = msg2
        if msg1 == '1000':
            self.msg1 = "- -"
        if msg2 == '1000':
            self.msg2 = "- -"
        self.text_size = text_size
        self.text = text
        self.color = color
        self.font1 = pygame.font.SysFont('comicsans', 20, True)
        self.font = pygame.font.SysFont('comicsans', self.text_size, True)
        Button.buttons.append(self)

    def draw(self, win, bg):
        pygame.draw.rect(win, self.color, self.rect)
        if bg == self.color:
            pygame.draw.rect(win, RED, (self.rect.x - 2, self.rect.y - 2, self.rect.width + 4, self.rect.height + 4), 2)
        else:
            pygame.draw.rect(win, self.color, self.rect)
        text = self.font.render(self.text, 3, self.text_color)
        win.blit(text, (int(self.rect.x) + self.rect.width//2 - text.get_width()//2, int(self.rect.y) + self.rect.height//2 - text.get_height()//2 + 1))

    def go(self):
        if self.action is not None:
            while len(Button.buttons) > 0:
                for button in Button.buttons:
                    Button.buttons.remove(button)
                    del button
            while len(Arrow.arrows) > 0:
                for arrow in Arrow.arrows:
                    Arrow.arrows.remove(arrow)
                    del arrow
            self.action(level=self.level)

    def show(self, win, pos):
        if self.msg1 != '':
            pygame.draw.rect(win, GREY, (pos[0], pos[1] - 40, 50, 40))
            text = self.font1.render(self.msg1, 1, RED)
            win.blit(text, (pos[0] + 25 - text.get_width() // 2, pos[1] - 30 - text.get_height() // 2))
            text = self.font1.render(self.msg2, 1, RED)
            win.blit(text, (pos[0] + 25 - text.get_width() // 2, pos[1] - 10 - text.get_height() // 2))

