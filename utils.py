import pygame
import os
import random
from functools import *

pygame.init()
clock = pygame.time.Clock()
FPS = 120
os.environ["SDL_VIDEO_CENTERED"] = '1'
font = pygame.font.SysFont('comicsans', 50, True)
sfont = pygame.font.SysFont('comicsans', 30, True)
FONT = pygame.font.SysFont('comicsans', 100, True)

BLACK = (0, 0, 0)
RED = (250, 10, 10)
DRED = (150, 0, 0)
GREEN = (10, 250, 10)
BLUE = (10, 10, 250)
WHITE = (255, 255, 255)
GOLD = (212, 175, 55)
DGOLD = (180, 140, 10)
BROWN = (76, 53, 41)
SKY = (71, 151, 202)
GREY = (122, 122, 122)
DGREY = (100, 100, 100)
PINK = (250, 25, 150)
PURPLE = (138, 43, 226)
ORANGE = (255, 140, 0)
EMERALD = (0, 128, 0)
YELLOW = "yellow"

colors = [GREY, WHITE, BLACK, GREEN, PINK, BLUE, SKY, PURPLE, ORANGE, YELLOW, GOLD]


def find_bound(walls):
    minx = 0
    maxx = 0
    tall = 0
    for wall in walls:
        minx = min(minx, wall.rect.x)
        maxx = max(maxx, wall.rect.right)
        tall = min(tall, wall.rect.y)
    return [minx, maxx, abs(tall) + 585]


def move(player, movement, walls, coins, bg):
    mx = 0
    my = 0
    for wall in walls:
        wall.rect.x += movement[0]
        wall.x += movement[0]
    collides = check_collisions(player, walls, bg)
    for wall in collides:
        if movement[0] < 0:
            if abs(player.right - wall.left) > mx:
                mx = wall.left - player.right
        if movement[0] > 0:
            if abs(wall.right - player.left) > mx:
                mx = wall.right - player.left
    for wall in walls:
        wall.rect.x -= mx
        wall.x -= mx
    for wall in walls:
        wall.rect.y += movement[1]
        wall.y += movement[1]
    collides = check_collisions(player, walls, bg)
    for wall in collides:
        if movement[1] < 0:
            if abs(player.bottom - wall.top) > my:
                my = wall.top - player.bottom
        if movement[1] > 0:
            if abs(wall.bottom - player.top) > my:
                my = wall.bottom - player.top
    for coin in coins:
        coin.y -= my
        coin.rect.y -= my
        coin.y += movement[1]
        coin.rect.y += movement[1]
        coin.x += movement[0]
        coin.rect.x += movement[0]
        coin.x -= mx
        coin.rect.x -= mx
    for wall in walls:
        wall.rect.y -= my
        wall.y -= my
    return [movement[0] - mx, movement[1] - my]

def check_collisions(player, walls, bg):
    collides = []
    for wall in walls:
        if bg != wall.color and player.colliderect(wall.rect):
            collides.append(wall.rect)
    return collides

# @lru_cache()
def get_levels_list():
    from walls import Wall, Coin

    levels_list = []

    for file_path in sorted(os.listdir("levels")):
        temp = []
        with open(os.path.join("levels", file_path), 'r') as file:
            lines = file.readlines()
        for line in lines:
            temp.append(eval(line.strip()))
        levels_list.append(temp)
    return levels_list

class Particle:
    particles = []

    def __init__(self, x, y, color):
        self.x = x
        self.y = y
        self.xfactor = random.random() * 2 - 1
        self.yfactor = random.random() * 2 - 1
        self.size = 10
        self.color = color
        Particle.particles.append(self)

    def x_vel(self):
        return self.size * self.xfactor

    def y_vel(self):
        return self.size * self.yfactor

    def update(self):
        self.size -= 0.15
        self.x += self.x_vel()
        self.y -= self.y_vel()
        if self.size <= 0:
            Particle.particles.remove(self)
            del self

    def draw(self, win):
        pygame.draw.circle(win, self.color, (int(self.x), int(self.y)), int(round(self.size)))


class MiniWall:
    def __init__(self, x, y, width, height, color, tx=0, ty=0, mx=0, my=0, msx=0, msy=0):
        self.rect = pygame.Rect(x, y, width, height)
        self.color = color
        self.move_x = mx
        self.move_y = my
        self.move_sx = msx
        self.move_sy = msy
        self.tx = tx
        self.ty = ty
        self.moved_x = 0
        self.moved_y = 0

    def draw(self, bg, win):
        if abs(self.moved_x) < self.move_x:
            self.rect.x += self.move_sx
            self.moved_x += self.move_sx
        else:
            self.move_sx *= -1
            self.moved_x = 0
        if abs(self.moved_y) < self.move_y:
            self.rect.y += self.move_sy
            self.moved_y += self.move_sy
        else:
            self.move_sy *= -1
            self.moved_y = 0
        pygame.draw.rect(win, RED, self.rect, 1)
        if bg != self.color:
            pygame.draw.rect(win, self.color, self.rect)

