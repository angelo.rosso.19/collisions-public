import pygame
from utils import *
from player import *
from walls import *

def dev(level="levels/test.txt"):
    walls = []
    with open(level, 'r') as file:
        lines = file.readlines()
        for line in lines:
            walls.append(eval(line.strip()))
    if not walls:
        walls = [Wall(-50000, 585, 100000, 15, GREY)]
    with open('settings.txt', 'r') as file:
        settings = file.readlines()
    color = eval(settings[0])
    WINDOWWIDTH = int(settings[1])
    WINDOWHEIGHT = int(settings[2])
    win = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.RESIZABLE)
    jon = Player(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color)
    flip = 120
    sped = 4.1
    g = 0.1
    gravity = 0
    g_on = False
    timer = 0
    teleport_timer = 0
    wait_point = None
    held_well = None
    selected = None
    x_speed = 0
    y_speed = 0
    tot_x_mov = 0
    tot_y_mov = 0
    coins = []
    shift = False
    paused = False
    run = True
    place_color = BLACK
    while run:
        if timer < flip * 2:
            bg_color = WHITE
        else:
            bg_color = BLACK
            if timer >= flip * 4:
                timer = 0
        win.fill(bg_color)
        if not paused:
            timer += 1
        clock.tick(FPS)
        if g_on:
            gravity += g
        grounded = False
        jump_factor = 1
        keys = pygame.key.get_pressed()
        if keys[pygame.K_DOWN]:
            if selected is not None:
                if shift:
                    selected.height += 2
                    selected.rect.height += 2
                else:
                    selected.y += 2
                    selected.rect.y += 2
            else:
                y_speed += sped
        if keys[pygame.K_UP]:
            if selected is not None:
                if shift:
                    if selected.rect.height > 2:
                        selected.height -= 2
                        selected.rect.height -= 2
                else:
                    selected.y -= 2
                    selected.rect.y -= 2
            else:
                gravity = 0
                y_speed -= sped
        if keys[pygame.K_RIGHT]:
            if selected is not None:
                if shift:
                    selected.width += 2
                    selected.rect.width += 2
                else:
                    selected.x += 2
                    selected.rect.x += 2
            else:
                x_speed += sped
        if keys[pygame.K_LEFT]:
            if selected is not None:
                if shift:
                    if selected.rect.width > 2:
                        selected.width -= 2
                        selected.rect.width -= 2
                else:
                    selected.x -= 2
                    selected.rect.x -= 2
            else:
                x_speed -= sped
        if keys[pygame.K_BACKSPACE]:
            if selected is not None:
                walls.remove(selected)
                del selected
                selected = None
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                exit()
            if event.type == pygame.VIDEORESIZE:
                x_speed -= (event.w - WINDOWWIDTH) / 2
                y_speed -= (event.h - WINDOWHEIGHT) / 2
                jon.rect.x += int(round((event.w - WINDOWWIDTH) / 2))
                jon.rect.y += int(round((event.h - WINDOWHEIGHT) / 2))
                WINDOWWIDTH = event.w
                WINDOWHEIGHT = event.h
                win = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.RESIZABLE)
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_r:
                    move(jon.rect, [-tot_x_mov, -tot_y_mov], walls, coins, bg_color)
                    for wall in walls:
                        if wall.color == YELLOW:
                            wall.rect.x = wall.x
                            wall.show = True
                    tot_x_mov = 0
                    tot_y_mov = 0
                    teleport_timer = 0
                    timer = 0
                    x_speed = 0
                    y_speed = 0
                    gravity = 0

                if event.key == pygame.K_c:
                    walls = [Wall(-50000, 585, 100000, 15, GREY)]
                if event.key == pygame.K_f:
                    move(jon.rect, [-50000 - walls[0].x, 585 - walls[0].y], walls, coins, bg_color)
                if event.key == pygame.K_p:
                    paused = not paused
                if event.key == pygame.K_s:
                    move(jon.rect, [-50000 - walls[0].x, 585 - walls[0].y], walls, coins, bg_color)
                    file_name = input("Enter level name: ")
                    with open(os.path.join("levels", file_name) + ".txt", "w+") as file:
                        for wall in walls:
                            if type(wall.color) == str:
                                wall.color = wall.color.upper()
                            file.write(f"Wall({wall.x}, {wall.y}, {wall.width}, {wall.height}, {wall.color}, tx={wall.tx}, ty={wall.ty}, mx=0, my=0, msx=0, msy=0, ghost=False, show=True)\n")
                if event.key == pygame.K_g:
                    if g_on:
                        g_on = False
                    else:
                        g_on = True
                if event.key == pygame.K_RETURN:
                    if shift:
                        shift = False
                    else:
                        shift = True
            if event.type == pygame.MOUSEBUTTONUP:
                pos = pygame.mouse.get_pos()
                if event.button == 1:
                    color_change = False
                    for i in range(len(colors)):
                        r = pygame.Rect([5 + i * (WINDOWWIDTH / len(colors)), WINDOWHEIGHT - 25, 20, 20])
                        if r.collidepoint(pos):
                            color_change = True
                            place_color = colors[i]
                            if selected:
                                selected.color = place_color
                    for wall in walls:
                        if wall.rect.collidepoint(pos):
                            color_change = True
                            selected = wall
                    if selected is None:
                        if not color_change:
                            if place_color == BLUE:
                                wait_point = pos
                                place_color = SKY
                            elif place_color == SKY:
                                if wait_point is not None:
                                    walls.append(Wall(wait_point[0], wait_point[1], 100, 20, BLUE, pos[0] - wait_point[0], pos[1] - wait_point[1]))
                                    walls.append(Wall(pos[0], pos[1], 100, 20, place_color))
                                    wait_point = None
                            else:
                                walls.append(Wall(pos[0], pos[1], 100, 20, place_color))
                    else:
                        if not color_change:
                            selected = None

        for wall in walls:
            if (
                    wall.rect.left == jon.rect.right or wall.rect.right == jon.rect.left) and jon.rect.bottom - jon.rect.height // 2 <= wall.rect.top <= jon.rect.bottom and wall.color != bg_color:
                if (int(x_speed // 1) > 0 and jon.rect.right <= wall.rect.left) or (int(x_speed // 1) < 0 and jon.rect.left >= wall.rect.right):
                    y_speed -= jon.rect.bottom - wall.rect.top
                    gravity = 0
            if (
                    (
                            wall.rect.left < jon.rect.left < wall.rect.right or wall.rect.left < jon.rect.right < wall.rect.right) or (
                            jon.rect.left < wall.rect.left < jon.rect.right or jon.rect.left < wall.rect.right < jon.rect.right)) and wall.color != bg_color:
                if jon.rect.bottom == wall.rect.top:
                    grounded = True
                    gravity = 0
                    x_speed += wall.move_sx
                    y_speed += wall.move_sy
                    if wall.color == YELLOW and keys[pygame.K_SPACE]:
                        wall.show = False
                        wall.rect.x = 1000000
                        # walls.remove(wall)
                    if wall.color == PINK:
                        jump_factor = 2
                    if wall.color == BROWN:
                        jump_factor = 0.66
                    if wall.color == BLUE:
                        if bg_color == BLACK:
                            teleport_timer += 1
                        if teleport_timer == flip / 2:
                            teleport_timer = 0
                            x_speed += wall.tx
                            y_speed += wall.ty
                    if wall.color == GREEN:
                        jump_factor = 0
                    if wall.color == PURPLE:
                        paused = True
                    if wall.color == ORANGE:
                        paused = False
                if jon.rect.top == wall.rect.bottom:
                    gravity = 0.1
            wall.draw(bg_color, win)
        if selected:
            pygame.draw.rect(win, EMERALD, [selected.x, selected.y, selected.width, selected.height], 2)
        if keys[pygame.K_SPACE] and grounded:
            gravity -= g * 62.95 * jump_factor
        # if pygame.mouse.get_pressed()[0]:
        #     for i in range(1):
        #         Particle(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1],
        #                  (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255)))
        for coin in coins:
            coin.draw(win)
        pygame.draw.circle(win, GREY, (40, 40), 19, 2)
        pygame.draw.circle(win, GREY, (80, 40), 19, 2)
        pygame.draw.circle(win, GREY, (120, 40), 19, 2)
        if jon.coin1:
            pygame.draw.circle(win, GOLD, (40, 40), 20)
            pygame.draw.circle(win, DGOLD, (40, 40), 20 - 20 // 3)
            pygame.draw.line(win, GOLD, (40 - 20 // 2, 40), (40 + 20 // 2, 40), 5)
        if jon.coin2:
            pygame.draw.circle(win, GOLD, (80, 40), 20)
            pygame.draw.circle(win, DGOLD, (80, 40), 20 - 20 // 3)
            pygame.draw.line(win, GOLD, (80 - 20 // 2, 40), (80 + 20 // 2, 40), 5)
        if jon.coin3:
            pygame.draw.circle(win, GOLD, (120, 40), 20)
            pygame.draw.circle(win, DGOLD, (120, 40), 20 - 20 // 3)
            pygame.draw.line(win, GOLD, (120 - 20 // 2, 40), (120 + 20 // 2, 40), 5)
        y_speed += gravity
        for particle in Particle.particles:
            particle.update()
            particle.y += 2
            particle.draw(win)
        tot_mov = move(jon.rect, [-int(x_speed // 1), -int(y_speed // 1)], walls, coins, bg_color)
        tot_x_mov += tot_mov[0]
        tot_y_mov += tot_mov[1]
        x_speed -= x_speed // 1
        y_speed -= y_speed // 1
        pygame.draw.rect(win, GREY, (WINDOWWIDTH - 60, 10, 50, 10), 2)
        if bg_color == BLACK:
            pygame.draw.rect(win, WHITE, (WINDOWWIDTH - 60, 10, 50 * (timer % (flip*2)) // (flip*2), 10))
        if bg_color == WHITE:
            pygame.draw.rect(win, BLACK,
                             (WINDOWWIDTH - 10 - 50 * (timer % (flip*2)) // (flip*2), 10, 50 * (timer % (flip*2)) // (flip*2), 10))
        jon.draw(win)
        pygame.draw.circle(win, place_color, (WINDOWWIDTH - 15, WINDOWHEIGHT - 15), 10)
        for i in range(len(colors)):
            pygame.draw.rect(win, colors[i], [5 + i * (WINDOWWIDTH / len(colors)), WINDOWHEIGHT - 25, 20, 20], border_radius=5)
            pygame.draw.rect(win, GREY, [5 + i * (WINDOWWIDTH / len(colors)), WINDOWHEIGHT - 25, 20, 20], 2, border_radius=5)
        pygame.display.update()


if __name__ == "__main__":
    dev()
