import pygame
import menu
from utils import *
from button import *


def settings(level=0):
    settings = True
    WINDOWWIDTH = 800
    WINDOWHEIGHT = 600
    clicked_wid = False
    clicked_hi = False
    clicked_r = False
    clicked_g = False
    clicked_b = False
    win = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.RESIZABLE)
    text = FONT.render('SETTINGS', True, RED)
    Button(WINDOWWIDTH//2 - 75, 140, 150, 30, WHITE, 'MAIN MENU', GOLD, 30, menu.menu, 0, "", "")
    coin_c = Button(100, 140, 200, 30, GOLD, 'Coin Confetti', WHITE, 30, None, 0, "", "")
    win_c = Button(500, 140, 190, 30, GOLD, 'Win Confetti', WHITE, 30, None, 0, "", "")
    with open('settings.txt', 'r') as file:
        settings_choices = file.readlines()
    color = eval(settings_choices[0])
    win_wid = int(settings_choices[1])
    win_hi = int(settings_choices[2])
    win_confetti = eval(settings_choices[3])
    coin_particles = eval(settings_choices[4])
    g_show = eval(settings_choices[6])
    sg_show = eval(settings_choices[7])
    if g_show:
        g_c = GREEN
    else:
        g_c = RED
    if sg_show:
        sg_c = GREEN
    else:
        sg_c = RED
    ghosts = Button(WINDOWWIDTH - 75, WINDOWHEIGHT - 75, 50, 50, g_c, '', WHITE, 20, None, 0, "", "")
    sghosts = Button(25, WINDOWHEIGHT - 75, 50, 50, sg_c, '', WHITE, 20, None, 0, "", "")
    textw = font.render(str(win_wid), True, BLACK)
    texth = font.render(str(win_hi), True, BLACK)
    textc = font.render("Player Color", True, WHITE)
    textr = font.render("Screen Size", True, WHITE)
    textx = font.render("X", True, WHITE)
    textg = sfont.render("Ghosts", True, WHITE)
    textsg = sfont.render("sGhosts", True, WHITE)
    while settings:
        win.fill(GREY)
        win.blit(text, (WINDOWWIDTH // 2 - text.get_width() // 2, text.get_height() // 2 + 15))
        pygame.draw.rect(win, WHITE, (WINDOWWIDTH//3 - 50, 510, 100, 50))
        pygame.draw.rect(win, WHITE, (WINDOWWIDTH // 3 * 2 - 50, 510, 100, 50))
        pygame.draw.rect(win, BLACK, (100, 200, 590, 185), 2)
        pygame.draw.rect(win, BLACK, (100, 400, 590, 185), 2)
        win.blit(textw, (WINDOWWIDTH // 3 - textw.get_width() // 2, 520))
        win.blit(textsg, (50 - textsg.get_width()//2, 500))
        win.blit(textg, (WINDOWWIDTH - 50 - textg.get_width() // 2, 500))
        win.blit(texth, (WINDOWWIDTH // 3 * 2 - texth.get_width() // 2, 520))
        win.blit(textc, (WINDOWWIDTH // 2 - textc.get_width() // 2, 210))
        win.blit(textr, (WINDOWWIDTH // 2 - textr.get_width() // 2, 410))
        win.blit(textx, (WINDOWWIDTH // 2 - textx.get_width() // 2, 520))
        pygame.draw.circle(win, color, (WINDOWWIDTH//2, WINDOWHEIGHT//2), 30)

        pygame.draw.rect(win, BLACK, (WINDOWWIDTH // 3 - 88, 470, 176, 10))
        pygame.draw.rect(win, WHITE, (WINDOWWIDTH // 3 + (win_wid-400) // 5 - 88 - 15, 465, 30, 20))
        pygame.draw.rect(win, GREY, (WINDOWWIDTH // 3 + (win_wid-400) // 5 + 4 - 88 - 15, 468, 2, 14))
        pygame.draw.rect(win, GREY, (WINDOWWIDTH // 3 + (win_wid-400) // 5 + 9 - 88 - 15, 468, 2, 14))
        pygame.draw.rect(win, GREY, (WINDOWWIDTH // 3 + (win_wid-400) // 5 + 14 - 88 - 15, 468, 2, 14))
        pygame.draw.rect(win, GREY, (WINDOWWIDTH // 3 + (win_wid-400) // 5 + 19 - 88 - 15, 468, 2, 14))
        pygame.draw.rect(win, GREY, (WINDOWWIDTH // 3 + (win_wid-400) // 5 + 24 - 88 - 15, 468, 2, 14))

        pygame.draw.rect(win, BLACK, (WINDOWWIDTH // 3 * 2 - 69, 470, 138, 10))
        pygame.draw.rect(win, WHITE, (int(WINDOWWIDTH // 3 * 2 + (win_hi-400) // 2.1 - 69 - 15), 465, 30, 20))
        pygame.draw.rect(win, GREY, (int(WINDOWWIDTH // 3 * 2 + (win_hi-400) // 2.1 + 4 - 69 - 15), 468, 2, 14))
        pygame.draw.rect(win, GREY, (int(WINDOWWIDTH // 3 * 2 + (win_hi-400) // 2.1 + 9 - 69 - 15), 468, 2, 14))
        pygame.draw.rect(win, GREY, (int(WINDOWWIDTH // 3 * 2 + (win_hi-400) // 2.1 + 14 - 69 - 15), 468, 2, 14))
        pygame.draw.rect(win, GREY, (int(WINDOWWIDTH // 3 * 2 + (win_hi-400) // 2.1 + 19 - 69 - 15), 468, 2, 14))
        pygame.draw.rect(win, GREY, (int(WINDOWWIDTH // 3 * 2 + (win_hi-400) // 2.1 + 24 - 69 - 15), 468, 2, 14))

        pygame.draw.rect(win, BLACK, (WINDOWWIDTH // 4 * 1 - 69, 350, 127, 10))
        pygame.draw.rect(win, RED, (WINDOWWIDTH // 4 * 1 + (color[0]) // 2 - 69 - 15, 345, 30, 20))
        pygame.draw.rect(win, DRED, (WINDOWWIDTH // 4 * 1 + (color[0]) // 2 + 4 - 69 - 15, 348, 2, 14))
        pygame.draw.rect(win, DRED, (WINDOWWIDTH // 4 * 1 + (color[0]) // 2 + 9 - 69 - 15, 348, 2, 14))
        pygame.draw.rect(win, DRED, (WINDOWWIDTH // 4 * 1 + (color[0]) // 2 + 14 - 69 - 15, 348, 2, 14))
        pygame.draw.rect(win, DRED, (WINDOWWIDTH // 4 * 1 + (color[0]) // 2 + 19 - 69 - 15, 348, 2, 14))
        pygame.draw.rect(win, DRED, (WINDOWWIDTH // 4 * 1 + (color[0]) // 2 + 24 - 69 - 15, 348, 2, 14))

        pygame.draw.rect(win, BLACK, (WINDOWWIDTH // 4 * 2 - 69, 350, 127, 10))
        pygame.draw.rect(win, GREEN, (WINDOWWIDTH // 4 * 2 + (color[1]) // 2 - 69 - 15, 345, 30, 20))
        pygame.draw.rect(win, EMERALD, (WINDOWWIDTH // 4 * 2 + (color[1]) // 2 + 4 - 69 - 15, 348, 2, 14))
        pygame.draw.rect(win, EMERALD, (WINDOWWIDTH // 4 * 2 + (color[1]) // 2 + 9 - 69 - 15, 348, 2, 14))
        pygame.draw.rect(win, EMERALD, (WINDOWWIDTH // 4 * 2 + (color[1]) // 2 + 14 - 69 - 15, 348, 2, 14))
        pygame.draw.rect(win, EMERALD, (WINDOWWIDTH // 4 * 2 + (color[1]) // 2 + 19 - 69 - 15, 348, 2, 14))
        pygame.draw.rect(win, EMERALD, (WINDOWWIDTH // 4 * 2 + (color[1]) // 2 + 24 - 69 - 15, 348, 2, 14))

        pygame.draw.rect(win, BLACK, (WINDOWWIDTH // 4 * 3 - 69, 350, 127, 10))
        pygame.draw.rect(win, BLUE, (WINDOWWIDTH // 4 * 3 + (color[2]) // 2 - 69 - 15, 345, 30, 20))
        pygame.draw.rect(win, SKY, (WINDOWWIDTH // 4 * 3 + (color[2]) // 2 + 4 - 69 - 15, 348, 2, 14))
        pygame.draw.rect(win, SKY, (WINDOWWIDTH // 4 * 3 + (color[2]) // 2 + 9 - 69 - 15, 348, 2, 14))
        pygame.draw.rect(win, SKY, (WINDOWWIDTH // 4 * 3 + (color[2]) // 2 + 14 - 69 - 15, 348, 2, 14))
        pygame.draw.rect(win, SKY, (WINDOWWIDTH // 4 * 3 + (color[2]) // 2 + 19 - 69 - 15, 348, 2, 14))
        pygame.draw.rect(win, SKY, (WINDOWWIDTH // 4 * 3 + (color[2]) // 2 + 24 - 69 - 15, 348, 2, 14))

        pos = pygame.mouse.get_pos()

        if clicked_wid:
            if WINDOWWIDTH // 3 - 88 <= pos[0] <= WINDOWWIDTH // 3 - 88 + 176:
                win_wid = (pos[0] - WINDOWWIDTH // 3 + 88) * 5 + 400
            elif pos[0] > WINDOWWIDTH // 3 - 88 + 176:
                win_wid = 1280
            else:
                win_wid = 400
            textw = font.render(str(win_wid), True, BLACK)
        if clicked_hi:
            if WINDOWWIDTH // 3 * 2 - 69 <= pos[0] <= WINDOWWIDTH // 3 * 2 - 69 + 138:
                win_hi = int((pos[0] - WINDOWWIDTH // 3 * 2 + 69) * 2.1 + 400)
            elif pos[0] > WINDOWWIDTH // 3 * 2 - 69 + 138:
                win_hi = 690
            else:
                win_hi = 400
            texth = font.render(str(win_hi), True, BLACK)
        if clicked_r:
            if WINDOWWIDTH // 4 * 1 - 69 <= pos[0] <= WINDOWWIDTH // 4 * 1 - 69 + 127:
                color[0] = (pos[0] - (WINDOWWIDTH // 4 * 1 - 69)) * 2
            elif pos[0] > (WINDOWWIDTH // 4 * 1) - 69 + 127:
                color[0] = 255
            else:
                color[0] = 0
        if clicked_g:
            if WINDOWWIDTH // 4 * 2 - 69 <= pos[0] <= WINDOWWIDTH // 4 * 2 - 69 + 127:
                color[1] = (pos[0] - (WINDOWWIDTH // 4 * 2 - 69)) * 2
            elif pos[0] > (WINDOWWIDTH // 4 * 2) - 69 + 127:
                color[1] = 255
            else:
                color[1] = 0
        if clicked_b:
            if WINDOWWIDTH // 4 * 3 - 69 <= pos[0] <= WINDOWWIDTH // 4 * 3 - 69 + 127:
                color[2] = (pos[0] - (WINDOWWIDTH // 4 * 3 - 69)) * 2
            elif pos[0] > (WINDOWWIDTH // 4 * 3) - 69 + 127:
                color[2] = 255
            else:
                color[2] = 0
        if g_show:
            ghosts.color = GREEN
        else:
            ghosts.color = RED
        if sg_show:
            sghosts.color = GREEN
        else:
            sghosts.color = RED
        for button in Button.buttons:
            button.draw(win, GREY)
            if button.rect.collidepoint(pos):
                button.show(win, pos)
        pygame.draw.rect(win, BLACK, (WINDOWWIDTH // 2 - 75, 140, 150, 30), 2)
        if win_confetti:
            pygame.draw.rect(win, GREEN, win_c.rect, 2)
        else:
            pygame.draw.rect(win, RED, win_c.rect, 2)
        if coin_particles:
            pygame.draw.rect(win, GREEN, coin_c.rect, 2)
        else:
            pygame.draw.rect(win, RED, coin_c.rect, 2)
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                settings = False
                pygame.quit()
                exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                keys = pygame.key.get_pressed()
                if WINDOWWIDTH // 3 + (win_wid-400) // 5 - 88 - 15 < pos[0] < WINDOWWIDTH // 3 + (win_wid-400) // 5 - 58 - 15 and 465 < pos[1] < 485:
                    clicked_wid = pygame.mouse.get_pressed()[0]
                if WINDOWWIDTH // 3 * 2 + (win_hi-400) // 2.1 - 69 - 15 < pos[0] < WINDOWWIDTH // 3 * 2 + (win_hi-400) // 2.1 - 39 - 15 and 465 < pos[1] < 485:
                    clicked_hi = pygame.mouse.get_pressed()[0]
                if WINDOWWIDTH // 4 * 1 + (color[0]) // 2 - 69 - 15 < pos[0] < WINDOWWIDTH // 4 * 1 + (color[0]) // 2 - 39 - 15 and 345 < pos[1] < 365:
                    clicked_r = pygame.mouse.get_pressed()[0]
                if WINDOWWIDTH // 4 * 2 + (color[1]) // 2 - 69 - 15 < pos[0] < WINDOWWIDTH // 4 * 2 + (color[1]) // 2 - 39 - 15 and 345 < pos[1] < 365:
                    clicked_g = pygame.mouse.get_pressed()[0]
                if WINDOWWIDTH // 4 * 3 + (color[2]) // 2 - 69 - 15 < pos[0] < WINDOWWIDTH // 4 * 3 + (color[2]) // 2 - 39 - 15 and 345 < pos[1] < 365:
                    clicked_b = pygame.mouse.get_pressed()[0]
                if coin_c.rect.collidepoint(pos):
                    coin_particles = not coin_particles
                if win_c.rect.collidepoint(pos):
                    win_confetti = not win_confetti
                if ghosts.rect.collidepoint(pos):
                    g_show = not g_show
                if sghosts.rect.collidepoint(pos):
                    sg_show = not sg_show
            if event.type == pygame.MOUSEBUTTONUP:
                clicked_hi = False
                clicked_wid = False
                clicked_r = False
                clicked_g = False
                clicked_b = False
                for button in Button.buttons:
                    if button.rect.collidepoint(pos):
                        settings_choices = ["{}\n".format(color), "{}\n".format(win_wid), "{}\n".format(win_hi), "{}\n".format(win_confetti), "{}\n".format(coin_particles), "1\n", f"{g_show}\n", f"{sg_show}\n"]
                        with open('settings.txt', 'w') as file:
                            file.writelines(settings_choices)
                        button.go()
