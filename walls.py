import pygame
from utils import *
from scores import *


class Wall:
    walls = []

    def __init__(self, x, y, width, height, color, tx=0, ty=0, mx=0, my=0, msx=0, msy=0, ghost=False, show=True):
        if not ghost:
            self.rect = pygame.Rect(x, y, width, height)
            # self.show = True
        else:
            self.rect = pygame.Rect(100000, 1000000, width, height)
        self.show = show
        self.color = color
        self.move_x = mx
        self.move_y = my
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.move_sx = msx
        self.move_sy = msy
        self.tx = tx
        self.ty = ty
        self.moved_x = 0
        self.moved_y = 0
        self.ghost = ghost
        Wall.walls.append(self.rect)

    def draw(self, bg, win):
        if abs(self.moved_x) < self.move_x:
            self.rect.x += self.move_sx
            self.x += self.move_sx
            self.moved_x += self.move_sx
        else:
            self.move_sx *= -1
            self.moved_x = 0
        if abs(self.moved_y) < self.move_y:
            self.rect.y += self.move_sy
            self.y += self.move_sy
            self.moved_y += self.move_sy
        else:
            self.move_sy *= -1
            self.moved_y = 0
        if self.show:
            if not self.ghost:
                pygame.draw.rect(win, RED, (self.x, self.y, self.width, self.height), 1)
                if bg != self.color:
                    pygame.draw.rect(win, self.color, (self.x, self.y, self.width, self.height))
            else:
                pygame.draw.rect(win, self.color, (int(self.x), int(self.y), self.width, self.height), 3)



class Coin:
    coins = []

    def __init__(self, x, y, n):
        self.x = x
        self.y = y
        self.r = 20
        self.num = n
        self.color = GOLD
        self.rect = pygame.Rect(self.x - self.r, self.y - self.r, self.r * 2, self.r * 2)
        Coin.coins.append(self.rect)

    def draw(self, win):
        pygame.draw.circle(win, self.color, (int(self.x), int(self.y)), self.r)
        pygame.draw.circle(win, DGOLD, (int(self.x), int(self.y)), self.r-self.r//3)
        pygame.draw.line(win, GOLD, (self.x - self.r // 2, self.y), (self.x + self.r // 2, self.y), 5)

