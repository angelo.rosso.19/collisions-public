import pygame
from utils import *
from button import *
import menu

def scores(level=0):
    scores = True
    WINDOWWIDTH = 800
    WINDOWHEIGHT = 600
    win = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.RESIZABLE)
    texts = []
    text_hs = FONT.render('HIGH SCORES', True, RED)
    Button(WINDOWWIDTH // 2 - 75, 140, 150, 30, WHITE, 'MAIN MENU', GOLD, 30, menu.menu, 0, "", "")
    score_range = [1, 10]
    page = 1
    with open("scores.txt", 'r') as file:
        scores_list = file.readlines()
    with open("sscores.txt", 'r') as file:
        any_scores_list = file.readlines()
    for i in range(len(scores_list)):
        texts.append(sfont.render(scores_list[i].strip(), True, BLACK))
        if i < len(any_scores_list):
            texts.append(sfont.render(any_scores_list[i].strip(), True, BLACK))
    back = pygame.Rect(100, 525, 40, 40)
    next = pygame.Rect(660, 525, 40, 40)
    lev_text = sfont.render("Level", True, BLACK)
    any_text = sfont.render("Any Percent", True, BLACK)
    full_text = sfont.render("100 Percent", True, BLACK)
    while scores:
        win.fill(GREY)
        # table
        pygame.draw.rect(win, WHITE, (100, 200, 600, 320))
        pygame.draw.line(win, DGREY, (100, 250), (700, 250), 4)
        pygame.draw.line(win, DGREY, (200, 200), (200, 520), 4)
        pygame.draw.line(win, DGREY, (450, 200), (450, 520), 4)
        pygame.draw.line(win, GREY, (100, 280), (700, 280), 3)
        pygame.draw.line(win, GREY, (100, 310), (700, 310), 3)
        pygame.draw.line(win, GREY, (100, 340), (700, 340), 3)
        pygame.draw.line(win, GREY, (100, 370), (700, 370), 3)
        pygame.draw.line(win, GREY, (100, 400), (700, 400), 3)
        pygame.draw.line(win, GREY, (100, 430), (700, 430), 3)
        pygame.draw.line(win, GREY, (100, 460), (700, 460), 3)
        pygame.draw.line(win, GREY, (100, 490), (700, 490), 3)
        pygame.draw.rect(win, BLACK, (100, 200, 600, 320), 4)
        pygame.draw.rect(win, WHITE, (100, 525, 40, 40))
        pygame.draw.rect(win, BLACK, (100, 525, 40, 40), 2)
        pygame.draw.polygon(win, BLACK, ((105, 545), (135, 530), (135, 560)))
        pygame.draw.rect(win, WHITE, (660, 525, 40, 40))
        pygame.draw.rect(win, BLACK, (660, 525, 40, 40), 2)
        pygame.draw.polygon(win, BLACK, ((665, 530), (665, 560), (695, 545)))

        if page == 1:
            score_range = [1, 10]
        if page == 2:
            score_range = [10, len(scores_list)]

        win.blit(text_hs, (WINDOWWIDTH // 2 - text_hs.get_width() // 2, text_hs.get_height() // 2 + 15))

        win.blit(lev_text, (150 - lev_text.get_width() // 2, 230 - lev_text.get_height() // 2))
        win.blit(any_text, (325 - any_text.get_width() // 2, 230 - any_text.get_height() // 2))
        win.blit(full_text, (575 - full_text.get_width() // 2, 230 - full_text.get_height() // 2))
        for i in range(score_range[0], score_range[1]):
            if i == len(any_scores_list):
                pleen = sfont.render("Total", True, BLACK)
            else:
                pleen = sfont.render("{}".format(i), True, BLACK)
            win.blit(pleen, (150 - pleen.get_width() // 2, pleen.get_height() // 2 + 245 + 30 * (i - score_range[0])))
            win.blit(texts[2 * i - 1], (325 - texts[2 * i - 1].get_width() // 2, texts[2 * i - 1].get_height() // 2 + 245 + 30 * (i - score_range[0])))
            win.blit(texts[2 * i], (575 - texts[2 * i].get_width() // 2, texts[2 * i].get_height() // 2 + 245 + 30 * (i - score_range[0])))
        pos = pygame.mouse.get_pos()
        for button in Button.buttons:
            button.draw(win, GREY)
            if button.rect.collidepoint(pos):
                button.show(win, pos)
        pygame.draw.rect(win, BLACK, (WINDOWWIDTH // 2 - 75, 140, 150, 30), 2)
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                scores = False
                pygame.quit()
                exit()
            if event.type == pygame.MOUSEBUTTONUP:
                if back.collidepoint(pos):
                    if page > 1:
                        page -= 1
                if next.collidepoint(pos):
                    if page < 2:
                        page += 1
            if event.type == pygame.MOUSEBUTTONUP:
                for button in Button.buttons:
                    if button.rect.collidepoint(pos):
                        button.go()