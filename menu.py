import pygame
from Public_collisions import *
from settings import *
from utils import *

def menu(level=0):
    menu = True
    timer = 0
    WINDOWWIDTH = 800
    WINDOWHEIGHT = 600
    text = FONT.render('COLLISIONS', True, RED)
    win = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.RESIZABLE)
    with open('scores.txt', 'r') as file:
        lines = file.readlines()
        for i in range(len(lines)):
            lines[i] = (lines[i]).strip()
    with open('sscores.txt', 'r') as file:
        liness = file.readlines()
        for i in range(len(liness)):
            liness[i] = (liness[i]).strip()
    Button(212, 200, 150, 30, WHITE, 'SETTINGS', GOLD, 30, settings, 0, "", "")
    Button(438, 200, 150, 30, WHITE, 'SCORES', GOLD, 30, scores, 0, "", "")
    Button(100, 300, 150, 30, BLACK, "Level 1", SKY, 30, game, 0, lines[1], liness[0])
    Button(100, 400, 150, 30, BLACK, "Level 2", GREEN, 30, game, 1, lines[2], liness[1])
    Button(100, 500, 150, 30, BLACK, "Level 3", PINK, 30, game, 2, lines[3], liness[2])
    Button(WINDOWWIDTH // 2 - 75, 300, 150, 30, BLACK, "Level 4", SKY, 30, game, 3, lines[4], liness[3])
    Button(WINDOWWIDTH // 2 - 75, 400, 150, 30, BLACK, "Level 5", GREEN, 30, game, 4, lines[5], liness[4])
    Button(WINDOWWIDTH // 2 - 75, 500, 150, 30, BLACK, "Level 6", PINK, 30, game, 5, lines[6], liness[5])
    Button(WINDOWWIDTH - 250, 300, 150, 30, BLACK, "Level 7", SKY, 30, game, 6, lines[7], liness[6])
    Button(WINDOWWIDTH - 250, 400, 150, 30, BLACK, "Level 8", GREEN, 30, game, 7, lines[8], liness[7])
    Button(WINDOWWIDTH - 250, 500, 150, 30, BLACK, "Level 9", PINK, 30, game, 8, lines[9], liness[8])
    Arrow(WINDOWWIDTH - 150, 175, 50, 70, RED, menu1, 'right')
    hotkeys = {pygame.K_1: 0, pygame.K_2: 1, pygame.K_3: 2, pygame.K_4: 3, pygame.K_5: 4, pygame.K_6: 5, pygame.K_7: 6,
               pygame.K_8: 7, pygame.K_9: 8}
    shifted_hotkeys = {pygame.K_0: 9, pygame.K_1: 10, pygame.K_2: 11, pygame.K_3: 3, pygame.K_4: 4, pygame.K_5: 5,
                       pygame.K_6: 6,
                       pygame.K_7: 7, pygame.K_8: 8}
    while menu:
        clock.tick(FPS)
        if timer < FPS * 2:
            bg_color = WHITE
        else:
            bg_color = BLACK
            if timer > FPS * 4:
                timer = 0
        # print(clock.get_fps())
        win.fill(bg_color)
        win.blit(text, (WINDOWWIDTH // 2 - text.get_width() // 2, text.get_height() // 2 + 25))
        pos = pygame.mouse.get_pos()
        for button in Button.buttons:
            button.draw(win, bg_color)
            if button.rect.collidepoint(pos):
                button.show(win, pos)
        for arrow in Arrow.arrows:
            arrow.draw(win)
        # print(timer)
        pygame.display.update()
        timer += 1
        keys = pygame.key.get_pressed()
        for event in pygame.event.get():

            if not keys[pygame.K_LSHIFT] and not keys[pygame.K_RSHIFT]:
                for key in hotkeys:
                    if keys[key] and timer:
                        level = hotkeys[key]
                        game(level)

            else:
                for key in shifted_hotkeys:
                    if keys[key] and timer:
                        level = shifted_hotkeys[key]
                        game(level)


            if event.type == pygame.QUIT:
                menu = False
                pygame.quit()
                exit()
            if event.type == pygame.VIDEORESIZE:
                WINDOWWIDTH = event.w
                WINDOWHEIGHT = event.h
                win = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.RESIZABLE)
            if event.type == pygame.MOUSEBUTTONUP:
                for button in Button.buttons:
                    if button.rect.collidepoint(pos):
                        button.go()
                    for arrow in Arrow.arrows:
                        if arrow.rect.collidepoint(pos):
                            arrow.go()


def menu1():
    menu1 = True
    timer = 0
    WINDOWWIDTH = 800
    WINDOWHEIGHT = 600
    text = FONT.render('COLLISIONS', True, RED)
    win = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.RESIZABLE)
    with open('scores.txt', 'r') as file:
        lines = file.readlines()
        for i in range(len(lines)):
            lines[i] = (lines[i]).strip()
    with open('sscores.txt', 'r') as file:
        liness = file.readlines()
        for i in range(len(liness)):
            liness[i] = (liness[i]).strip()
    Button(212, 200, 150, 30, WHITE, 'SETTINGS', GOLD, 30, settings, 0, "", "")
    Button(438, 200, 150, 30, WHITE, 'SCORES', GOLD, 30, scores, 0, "", "")
    Button(100, 300, 150, 30, BLACK, "Level 10", PURPLE, 30, game, 9, lines[10], liness[9])
    Button(100, 400, 150, 30, BLACK, "Level 11", ORANGE, 30, game, 10, lines[11], liness[10])
    Button(100, 500, 150, 30, BLACK, "Level 12", EMERALD, 30, game, 11, lines[12], liness[11])
    Button(WINDOWWIDTH // 2 - 75, 300, 150, 30, BLACK, "Level 13", PURPLE, 30, game, 12, lines[13], liness[12])
    Button(WINDOWWIDTH // 2 - 75, 400, 150, 30, BLACK, "Level 14", ORANGE, 30, game, 13, lines[14], liness[13])
    Button(WINDOWWIDTH // 2 - 75, 500, 150, 30, BLACK, "Level 15", EMERALD, 30, game, 14, lines[15], liness[14])
    # Button(WINDOWWIDTH - 250, 300, 150, 30, BLACK, "Level 16", PURPLE, 30, game, 15, lines[16], liness[15])
    # Button(WINDOWWIDTH - 250, 400, 150, 30, BLACK, "Level 17", ORANGE, 30, game, 16, lines[17], liness[16])
    # Button(WINDOWWIDTH - 250, 500, 150, 30, BLACK, "Level 18", EMERALD, 30, game, 17, lines[18], liness[17])
    Arrow(150, 175, 50, 70, RED, menu, 'left')
    while menu1:
        clock.tick(FPS)
        if timer < FPS * 2:
            bg_color = WHITE
        else:
            bg_color = BLACK
            if timer > FPS * 4:
                timer = 0
        # print(clock.get_fps())
        win.fill(bg_color)
        win.blit(text, (WINDOWWIDTH // 2 - text.get_width() // 2, text.get_height() // 2 + 25))
        pos = pygame.mouse.get_pos()
        for button in Button.buttons:
            button.draw(win, bg_color)
            if button.rect.collidepoint(pos):
                button.show(win, pos)
        for arrow in Arrow.arrows:
            arrow.draw(win)
        # print(timer)
        pygame.display.update()
        timer += 1
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                menu1 = False
                pygame.quit()
                exit()
            if event.type == pygame.VIDEORESIZE:
                WINDOWWIDTH = event.w
                WINDOWHEIGHT = event.h
                win = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.RESIZABLE)
            if event.type == pygame.MOUSEBUTTONUP:
                for button in Button.buttons:
                    if button.rect.collidepoint(pos):
                        button.go()
                for arrow in Arrow.arrows:
                    if arrow.rect.collidepoint(pos):
                        arrow.go()
