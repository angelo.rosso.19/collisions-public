from utils import *

class Player:
    def __init__(self, x, y, width, height, color):
        self.rect = pygame.Rect(x, y, width, height)
        self.coin1 = False
        self.coin2 = False
        self.coin3 = False
        self.hundred_percent = False
        self.color = color

    def draw(self, win):
        pygame.draw.rect(win, self.color, self.rect, border_radius=0)

