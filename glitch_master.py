import pygame
import os
import numpy as np
import random

# fix recursion depth

pygame.init()
clock = pygame.time.Clock()
FPS = 120
os.environ["SDL_VIDEO_CENTERED"] = '1'
font = pygame.font.SysFont('comicsans', 50, True)
sfont = pygame.font.SysFont('comicsans', 30, True)
FONT = pygame.font.SysFont('comicsans', 100, True)
Font = pygame.font.SysFont('comicsans', 80, True)

BLACK = (0, 0, 0)
RED = (250, 10, 10)
DRED = (150, 0, 0)
GREEN = (10, 250, 10)
BLUE = (10, 10, 250)
WHITE = (255, 255, 255)
GOLD = (212, 175, 55)
DGOLD = (180, 140, 10)
BROWN = (76, 53, 41)
SKY = (71, 151, 202)
GREY = (122, 122, 122)
DGREY = (100, 100, 100)
PINK = (250, 25, 150)
PURPLE = (138, 43, 226)
ORANGE = (255, 140, 0)
EMERALD = (0, 128, 0)


# def level_two():
#     return [Wall(-50000, 585, 100000, 15, GREY),
#             Wall(240, 425, 100, 20, BLACK),
#             Wall(350, 265, 100, 20, BLACK),
#             Wall(240, 105, 100, 20, BLACK),
#             Wall(350, -55, 100, 20, GREY),
#             Wall(240, -215, 100, 20, WHITE),
#             Wall(350, -375, 100, 20, WHITE),
#             Wall(240, -535, 100, 20, WHITE),
#             Wall(350, -695, 100, 20, WHITE),
#             Wall(240, -855, 100, 20, GREY),
#             Wall(350, -1015, 100, 20, BLACK),
#             Wall(240, -1175, 100, 20, BLACK),
#             Wall(350, -1335, 100, 20, BLACK),
#             Wall(240, -1495, 100, 20, BLACK),
#             Wall(350, -1655, 100, 20, BLACK),
#             Wall(240, -1815, 100, 20, GREY),
#             Wall(350, -1975, 100, 20, WHITE),
#             Wall(240, -2135, 100, 20, WHITE),
#             Wall(350, -2295, 100, 20, WHITE),
#             Wall(240, -2455, 100, 20, WHITE),
#             Wall(350, -2615, 100, 20, WHITE),
#             Wall(240, -2775, 100, 20, WHITE),
#             Wall(350, -2935, 100, 20, GREY),
#             Wall(240, -3095, 100, 20, BLACK),
#             Wall(350, -3255, 100, 20, BLACK),
#             Wall(240, -3415, 100, 20, BLACK),
#             Wall(350, -3575, 100, 20, BLACK),
#             Wall(240, -3735, 100, 20, BLACK),
#             Wall(350, -3895, 100, 20, BLACK),
#             Wall(240, -4055, 100, 20, BLACK),
#             Wall(350, -4215, 100, 20, GREY),]


def level_two():
    return [Wall(-50000, 585, 100000, 15, GREY),
            Wall(240, 425, 100, 20, GREY),
            Wall(350, 265, 100, 20, GREY),
            Wall(240, 105, 100, 20, GREY),
            Wall(350, -55, 100, 20, GREY)]


levels_list = [level_two]


def check_collisions(player, walls, bg):
    collides = []
    for wall in walls:
        if bg != wall.color and player.colliderect(wall.rect):
            collides.append(wall.rect)
    return collides


class Wall:
    walls = []

    def __init__(self, x, y, width, height, color, tx=0, ty=0, mx=0, my=0, msx=0, msy=0, ghost=False, show=True):
        if not ghost:
            self.rect = pygame.Rect(x, y, width, height)
        else:
            self.rect = pygame.Rect(100000, 1000000, width, height)
        self.show = show
        self.color = color
        self.move_x = mx
        self.move_y = my
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.move_sx = msx
        self.move_sy = msy
        self.tx = tx
        self.ty = ty
        self.moved_x = 0
        self.moved_y = 0
        self.ghost = ghost
        Wall.walls.append(self.rect)

    def draw(self, bg, win):
        if abs(self.moved_x) < self.move_x:
            self.rect.x += self.move_sx
            self.x += self.move_sx
            self.moved_x += self.move_sx
        else:
            self.move_sx *= -1
            self.moved_x = 0
        if abs(self.moved_y) < self.move_y:
            self.rect.y += self.move_sy
            self.y += self.move_sy
            self.moved_y += self.move_sy
        else:
            self.move_sy *= -1
            self.moved_y = 0
        if self.show:
            if not self.ghost:
                pygame.draw.rect(win, RED, (self.x, self.y, self.width, self.height), 1)
                if bg != self.color:
                    pygame.draw.rect(win, self.color, (self.x, self.y, self.width, self.height))
            else:
                pygame.draw.rect(win, self.color, (int(self.x), int(self.y), self.width, self.height), 3)


def move(player, movement, walls, coins, bg):
    mx = 0
    my = 0
    for wall in walls:
        wall.rect.x += movement[0]
        wall.x += movement[0]
    collides = check_collisions(player, walls, bg)
    for wall in collides:
        if movement[0] < 0:
            if abs(player.right - wall.left) > mx:
                mx = wall.left - player.right
        if movement[0] > 0:
            if abs(wall.right - player.left) > mx:
                mx = wall.right - player.left
    for wall in walls:
        wall.rect.x -= mx
        wall.x -= mx
    for wall in walls:
        wall.rect.y += movement[1]
        wall.y += movement[1]
    collides = check_collisions(player, walls, bg)
    for wall in collides:
        if movement[1] < 0:
            if abs(player.bottom - wall.top) > my:
                my = wall.top - player.bottom
        if movement[1] > 0:
            if abs(wall.bottom - player.top) > my:
                my = wall.bottom - player.top
    for coin in coins:
        coin.y -= my
        coin.rect.y -= my
        coin.y += movement[1]
        coin.rect.y += movement[1]
        coin.x += movement[0]
        coin.rect.x += movement[0]
        coin.x -= mx
        coin.rect.x -= mx
    for wall in walls:
        wall.rect.y -= my
        wall.y -= my


def generate(left, x, y):
    xx = 350
    list = []
    coin = random.random()
    if left:
        xx = 240
    if coin > 0.75:
        list.append(Coin(x + xx + 50, y - 25))
    list.append(Wall(x + xx, y, 100, 20, GREY))
    return list


class Player:
    def __init__(self, x, y, width, height, color):
        self.rect = pygame.Rect(x, y, width, height)
        self.coin1 = False
        self.coin2 = False
        self.coin3 = False
        self.hundred_percent = False
        self.color = color

    def draw(self, win):
        pygame.draw.rect(win, self.color, self.rect)


class Coin:
    coins = []

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.r = 20
        self.color = GOLD
        self.rect = pygame.Rect(self.x - self.r, self.y - self.r, self.r * 2, self.r * 2)
        Coin.coins.append(self)

    def draw(self, win):
        pygame.draw.circle(win, self.color, (int(self.x), int(self.y)), self.r)
        pygame.draw.circle(win, DGOLD, (int(self.x), int(self.y)), self.r - self.r // 3)
        pygame.draw.line(win, GOLD, (self.x - self.r // 2, self.y), (self.x + self.r // 2, self.y), 5)


def game(level=0):
    WINDOWWIDTH = 800
    WINDOWHEIGHT = 600
    top_height = 0
    loss = False
    left = True
    if not os.path.exists("score.txt"):
        with open("score.txt", 'w') as file:
            file.write("0")
    with open("score.txt", 'r') as file:
        high = int(file.read())
    text_hs = sfont.render(f"High Score: {high}", True, GOLD)
    run = True
    win_level = False
    level = level
    timer = 2000
    Coin.coins = []
    sped = 4.1
    g = 0.1
    teleport_timer = 0
    paused = False
    gravity = 0
    win_clock = 0
    if level == 0:
        fullrun_eligible = True
    else:
        fullrun_eligible = False
    level_time = 0
    x_speed = 0
    y_speed = 0
    fullrun_time = 0
    color = RED
    y_off = 0
    x_off = 0
    temp = 70
    win = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.RESIZABLE)
    jon = Player(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color)
    walls = levels_list[level]()
    for wall in walls:
        wall.rect.x += (WINDOWWIDTH - 800) // 2
        wall.rect.y += (WINDOWHEIGHT - 600) // 2
        wall.x += (WINDOWWIDTH - 800) // 2
        wall.y += (WINDOWHEIGHT - 600) // 2
    tot_x_mov = 0
    tot_y_mov = 0
    flip = 1000
    wall_og_x = walls[0].rect.x
    wall_og_y = walls[0].rect.y
    while run:
        caption = "Glitch-Master {}".format(walls[0].rect.y - 450)
        pygame.display.set_caption(caption)
        if timer < flip * 2:
            bg_color = WHITE
        else:
            bg_color = BLACK
            if timer >= flip * 4:
                timer = 0
        win.fill(WHITE)
        if not win_level:
            fullrun_time += 1
            if not paused:
                timer -= 1
            level_time += 1
        clock.tick(FPS)
        text_s = sfont.render(f"Score: {top_height}", True, BLUE)
        win.blit(text_s, (10, text_hs.get_height() + 20))
        win.blit(text_hs, (10, 10))
        gravity += g
        grounded = False
        jump_factor = 1
        keys = pygame.key.get_pressed()
        # if keys[pygame.K_DOWN]:
        #     y_speed += sped
        # if keys[pygame.K_UP]:
        #     gravity = 0
        #     y_speed -= 5*sped
        if walls[0].rect.y - 450 > top_height:
            top_height = walls[0].rect.y - 450
        if keys[pygame.K_r]:
            with open("score.txt", 'r') as file:
                hs = file.read()
            if top_height > int(hs):
                with open("score.txt", 'w') as file:
                    file.write(str(top_height))
            game()
            exit()
        if keys[pygame.K_RIGHT]:
            x_speed += sped
        if keys[pygame.K_LEFT]:
            x_speed -= sped
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    print(top_height)
            if event.type == pygame.VIDEORESIZE:
                x_speed -= (event.w - WINDOWWIDTH) / 2
                y_speed -= (event.h - WINDOWHEIGHT) / 2
                jon.rect.x += int(round((event.w - WINDOWWIDTH) / 2))
                jon.rect.y += int(round((event.h - WINDOWHEIGHT) / 2))
                WINDOWWIDTH = event.w
                WINDOWHEIGHT = event.h
                for wall in walls:
                    wall.rect.x += (WINDOWWIDTH - 800) // 2
                    wall.rect.y += (WINDOWHEIGHT - 600) // 2
                    wall.x += (WINDOWWIDTH - 800) // 2
                    wall.y += (WINDOWHEIGHT - 600) // 2
                win = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.RESIZABLE)
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                exit()
        for wall in walls:
            if (
                    wall.rect.left == jon.rect.right or wall.rect.right == jon.rect.left) and jon.rect.bottom - jon.rect.height // 2 <= wall.rect.top <= jon.rect.bottom and wall.color != bg_color:
                if (int(x_speed // 1) > 0 and jon.rect.right <= wall.rect.left) or (
                        int(x_speed // 1) < 0 and jon.rect.left >= wall.rect.right):
                    y_speed -= jon.rect.bottom - wall.rect.top
                    gravity = 0
            if (
                    (
                            wall.rect.left < jon.rect.left < wall.rect.right or wall.rect.left < jon.rect.right < wall.rect.right) or (
                            jon.rect.left < wall.rect.left < jon.rect.right or jon.rect.left < wall.rect.right < jon.rect.right)) and wall.color != bg_color:
                if jon.rect.bottom == wall.rect.top:
                    grounded = True
                    gravity = 0
                    x_speed += wall.move_sx
                    y_speed += wall.move_sy
                    if wall.color == PINK:
                        jump_factor = 2
                    if wall.color == BROWN:
                        jump_factor = 0.5
                    if wall.color == BLUE:
                        if bg_color == BLACK:
                            teleport_timer += 1
                        if teleport_timer == flip / 2:
                            teleport_timer = 0
                            x_speed += wall.tx
                            y_speed += wall.ty
                    if wall.color == GREEN:
                        jump_factor = 0
                    if wall.color == PURPLE:
                        paused = True
                    if wall.color == ORANGE:
                        paused = False
                    if wall.color == GOLD:
                        win_clock += 1
                        if win_clock == flip / 2:
                            win_level = True
                        if win_level:
                            keys = pygame.key.get_pressed()
                            if keys[pygame.K_SPACE]:
                                if level < len(levels_list) - 1:
                                    level += 1
                                gravity = 0
                                timer = 0
                                level_time = 0
                                win_clock = 0
                                teleport_timer = 0
                                paused = False
                                tot_x_mov = 0
                                tot_y_mov = 0
                                if not jon.hundred_percent:
                                    fullrun_eligible = False
                                jon.hundred_percent = False
                                grounded = False
                                win_level = False
                                walls = levels_list[level]()
                                for wall in walls:
                                    wall.rect.x += (WINDOWWIDTH - 800) // 2
                                    wall.rect.y += (WINDOWHEIGHT - 600) // 2
                                    wall.x += (WINDOWWIDTH - 800) // 2
                                    wall.y += (WINDOWHEIGHT - 600) // 2
                if jon.rect.top == wall.rect.bottom:
                    gravity = 0.1
        if keys[pygame.K_SPACE] and grounded:
            gravity -= g * 62.95 * jump_factor

        for wall in walls:
            wall.draw(bg_color, win)
        for coin in Coin.coins:
            coin.draw(win)

        if fullrun_eligible:
            pygame.draw.circle(win, GREEN, (WINDOWWIDTH - 20, WINDOWHEIGHT - 20), 10)
        else:
            pygame.draw.circle(win, RED, (WINDOWWIDTH - 20, WINDOWHEIGHT - 20), 10)

        y_speed += gravity

        if not win_level and not loss:
            move(jon.rect, [-int(x_speed // 1), -int(y_speed // 1)], walls, Coin.coins, bg_color)
            x_speed -= x_speed // 1
            y_speed -= y_speed // 1

        tot_x_mov += walls[0].rect.x - wall_og_x
        wall_og_x = walls[0].rect.x
        tot_y_mov += walls[0].rect.y - wall_og_y
        wall_og_y = walls[0].rect.y

        pygame.draw.rect(win, RED, (WINDOWWIDTH - 60, 10, 50, 10))
        pygame.draw.rect(win, GREY, (WINDOWWIDTH - 60, 10, 50 * (timer % (2000)) // (2000), 10))
        pygame.draw.rect(win, GREY, (WINDOWWIDTH - 60, 10, 50, 10), 2)
        jon.draw(win)
        if top_height > tot_y_mov + 135 + 160 or loss:
            if top_height <= 2000:
                loser = Font.render("YOU TRIED", True, RED)
            if 2000 < top_height <= 4000:
                loser = Font.render("YOU NEED PRACTICE", True, RED)
            if 4000 < top_height <= 6000:
                loser = Font.render("NOT BAD", True, RED)
            if 6000 < top_height < 8000:
                loser = Font.render("NICE WORK", True, RED)
            if top_height >= 8000:
                loser = font.render("YOU ARE A GLITCH MASTER", True, RED)
            paused = True
            win.blit(loser, (WINDOWWIDTH // 2 - loser.get_width() // 2, WINDOWHEIGHT // 2 - loser.get_height() // 2))
            loss = True
        if top_height > (-1 * walls[-4].rect.y + 540):  # temp + 250:
            temp += 160
            y_off += 1
            x_off += 1
            cy = random.randint(-y_off, y_off)
            cx = random.randint(-x_off, x_off)
            addition = 160 + cx
            if addition > 200:
                addition = 200
            list = generate(left, tot_x_mov + cy, walls[-1].rect.y - addition)
            for thing in list:
                if type(thing) is Wall:
                    walls.append(thing)
            left = not left
        for wall in walls[1:]:
            if wall.rect.y > 535:
                walls.remove(wall)
                del wall
        for coin in Coin.coins:
            if jon.rect.colliderect(coin.rect):
                Coin.coins.remove(coin)
                timer += 50
                del coin
        if timer > 2000:
            timer = 2000
        if timer < 0:
            loss = True
        for coin in Coin.coins:
            if coin.rect.y > 510:
                Coin.coins.remove(coin)
                del coin
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                with open("score.txt", 'r') as file:
                    hs = file.read()
                print(int(top_height))
                if top_height > int(hs):
                    with open("score.txt", 'w') as file:
                        file.write(str(top_height))
                run = False
                pygame.quit()
                exit()


if __name__ == "__main__":
    game()
