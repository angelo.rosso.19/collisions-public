import numpy as np
import menu
from walls import *
from dev_mode import *
from player import *

# maybe put person on minimap
# more levels!!

# boss fight with enemies that you cant touch
# inconsistant time
# powerups: hover for a litle bit,


# def level_one():  # Testy boi
#     return [Coin(900, 565, 1),
#             Coin(400, 200, 2),
#             Coin(400, -400, 3),
#             Wall(-50000, 585, 100000, 15, GREY),
#             Wall(200, 415, 100, 20, BROWN, 0, 0, 50, 0, 1, 0),
#             Wall(500, 250, 100, 20, BLACK),
#             Wall(200, 100, 100, 20, BLACK),
#             Wall(350, 0, 100, 20, BLACK),
#             Wall(500, 415, 100, 20, WHITE),
#             Wall(200, 250, 100, 20, WHITE),
#             Wall(500, 100, 100, 20, WHITE)]


def game(level=0):
    with open('settings.txt', 'r') as file:
        settings = file.readlines()
    color = eval(settings[0])
    WINDOWWIDTH = int(settings[1])
    WINDOWHEIGHT = int(settings[2])
    win_confetti = eval(settings[3])
    coin_particles = eval(settings[4])
    g_show = eval(settings[6])
    sg_show = eval(settings[7])
    if int(settings[5]) == 2:
        flip = 60
        any_perc = "fsscores.txt"
        full_finish = "fscores.txt"
        sghost_file = "fsGhosts/fsghost"
        ghost_file = "fGhosts/fghost"
    elif int(settings[5]) == 3:
        flip = 240
        any_perc = "lsscores.txt"
        full_finish = "lscores.txt"
        sghost_file = "lsGhosts/lsghost"
        ghost_file = "lGhosts/lghost"
    else:
        flip = 120
        any_perc = "sscores.txt"
        full_finish = "scores.txt"
        sghost_file = "sGhosts/sghost"
        ghost_file = "Ghosts/ghost"
    run = True
    win_level = False
    level = level
    timer = 0
    sped = 4.1
    g = 0.1
    teleport_timer = 0
    paused = False
    gravity = 0
    win_clock = 0
    if level == 0:
        fullrun_eligible = True
    else:
        fullrun_eligible = False
    level_time = 0
    x_speed = 0
    y_speed = 0
    fullrun_time = 0
    win = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.RESIZABLE)
    jon = Player(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color)
    levels_list = get_levels_list()
    walls = levels_list[level][3:]
    coins = levels_list[level][:3]
    hotkeys = {pygame.K_1: 0, pygame.K_2: 1, pygame.K_3: 2, pygame.K_4: 3, pygame.K_5: 4, pygame.K_6: 5, pygame.K_7: 6, pygame.K_8: 7, pygame.K_9: 8}
    shifted_hotkeys = {pygame.K_0: 9, pygame.K_1: 10, pygame.K_2: 11, pygame.K_3: 12, pygame.K_4: 13, pygame.K_5: 14, pygame.K_6: 6,
               pygame.K_7: 7, pygame.K_8: 8}
    for wall in walls:
        wall.rect.x += (WINDOWWIDTH - 800)//2
        wall.rect.y += (WINDOWHEIGHT - 600)//2
        wall.x += (WINDOWWIDTH - 800) // 2
        wall.y += (WINDOWHEIGHT - 600) // 2
    for coin in coins:
        coin.rect.x += (WINDOWWIDTH - 800) // 2
        coin.rect.y += (WINDOWHEIGHT - 600) // 2
        coin.x += (WINDOWWIDTH - 800) // 2
        coin.y += (WINDOWHEIGHT - 600) // 2
    mini_map = []
    boud = find_bound(walls[1:])
    tot_x_mov = 0
    tot_y_mov = 0
    for wall in walls[1:]:
        mini_map.append(MiniWall(int((wall.rect.x - boud[0])//13)+10, int((wall.rect.y - WINDOWHEIGHT)//10) + WINDOWHEIGHT, int(wall.rect.width//10), int(wall.rect.height//10), wall.color))
    high_score = False
    gx = walls[0].rect.x
    gy = walls[0].rect.y
    sghost_moves = []
    ghost_moves = []
    ghost_it = False
    sghost_it = False
    g_frame = 0
    if sg_show:
        with open("{}_{}.txt".format(sghost_file, level), 'r') as file:
            sg_lines = file.readlines()
            for i in range(len(sg_lines)):
                sg_lines[i] = eval(sg_lines[i].strip())
    if g_show:
        with open("{}_{}.txt".format(ghost_file, level), 'r') as file:
            g_lines = file.readlines()
            for i in range(len(g_lines)):
                g_lines[i] = eval(g_lines[i].strip())
    if settings[6]:
        ghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color, ghost=True, show=g_show)
        walls.append(ghost)
    if settings[7]:
        sghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color, ghost=True, show=sg_show)
        walls.append(sghost)
    while run:
        caption = "{}.{}".format(fullrun_time // 60, int((fullrun_time % 60) * (100 / 60)))
        pygame.display.set_caption(caption)
        if timer < flip * 2:
            bg_color = WHITE
        else:
            bg_color = BLACK
            if timer >= flip * 4:
                timer = 0
        win.fill(bg_color)
        if not win_level:
            fullrun_time += 1
            if not paused:
                timer += 1
            level_time += 1
        clock.tick(FPS)
        gravity += g
        grounded = False
        jump_factor = 1
        keys = pygame.key.get_pressed()
        # if keys[pygame.K_DOWN]:
        #     y_speed += sped
        # if keys[pygame.K_UP]:
        #     gravity = 0
        #     y_speed -= sped
        if keys[pygame.K_RIGHT]:
            x_speed += sped
        if keys[pygame.K_LEFT]:
            x_speed -= sped
        if keys[pygame.K_m]:
            menu.menu()
        if not keys[pygame.K_LSHIFT] and not keys[pygame.K_RSHIFT] and not win_level:
            for key in hotkeys:
                if keys[key] and timer:
                    level = hotkeys[key]
                    if level == 0:
                        fullrun_time = 0
                        fullrun_eligible = True
                    else:
                        fullrun_eligible = False
                    gravity = 0
                    level_time = 0
                    jon.coin1 = False
                    jon.coin2 = False
                    jon.coin3 = False
                    paused = False
                    tot_x_mov = 0
                    tot_y_mov = 0
                    mini_map = []
                    levels_list = get_levels_list()
                    walls = levels_list[level][3:]
                    coins = levels_list[level][:3]
                    boud = find_bound(walls[1:])
                    gx = walls[0].rect.x
                    gy = walls[0].rect.y
                    for wall in walls[1:]:
                        mini_map.append(MiniWall(int((wall.rect.x - boud[0]) // 13) + 10,
                                                 int((wall.rect.y - WINDOWHEIGHT) // 10) + WINDOWHEIGHT,
                                                 int(wall.rect.width // 10), int(wall.rect.height // 10), wall.color))
                    teleport_timer = 0
                    jon.hundred_percent = False
                    timer = 0
                    g_frame = 0
                    with open("sGhosts/sghost_{}.txt".format(level), 'r') as file:
                        sg_lines = file.readlines()
                        for i in range(len(sg_lines)):
                            sg_lines[i] = eval(sg_lines[i].strip())
                    with open("Ghosts/ghost_{}.txt".format(level), 'r') as file:
                        g_lines = file.readlines()
                        for i in range(len(g_lines)):
                            g_lines[i] = eval(g_lines[i].strip())
                    sghost_moves = []
                    ghost_moves = []
                    ghost_it = False
                    sghost_it = False
                    win_clock = 0
                    if settings[6]:
                        ghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color,
                                     ghost=True, show=g_show)
                        walls.append(ghost)
                    if settings[7]:
                        sghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color,
                                      ghost=True, show=sg_show)
                        walls.append(sghost)
                    for wall in walls[:-2]:
                        wall.rect.x += (WINDOWWIDTH - 800) // 2
                        wall.rect.y += (WINDOWHEIGHT - 600) // 2
                        wall.x += (WINDOWWIDTH - 800) // 2
                        wall.y += (WINDOWHEIGHT - 600) // 2
                    for coin in coins:
                        coin.rect.x += (WINDOWWIDTH - 800) // 2
                        coin.rect.y += (WINDOWHEIGHT - 600) // 2
                        coin.x += (WINDOWWIDTH - 800) // 2
                        coin.y += (WINDOWHEIGHT - 600) // 2
        elif not win_level:
            for key in shifted_hotkeys:
                if keys[key] and timer:
                    level = shifted_hotkeys[key]
                    fullrun_eligible = False
                    gravity = 0
                    level_time = 0
                    jon.coin1 = False
                    jon.coin2 = False
                    jon.coin3 = False
                    paused = False
                    tot_x_mov = 0
                    tot_y_mov = 0
                    mini_map = []
                    levels_list = get_levels_list()
                    walls = levels_list[level][3:]
                    coins = levels_list[level][:3]
                    boud = find_bound(walls[1:])
                    gx = walls[0].rect.x
                    gy = walls[0].rect.y
                    for wall in walls[1:]:
                        mini_map.append(MiniWall(int((wall.rect.x - boud[0]) // 13) + 10,
                                                 int((wall.rect.y - WINDOWHEIGHT) // 10) + WINDOWHEIGHT,
                                                 int(wall.rect.width // 10), int(wall.rect.height // 10), wall.color))
                    teleport_timer = 0
                    g_frame = 0
                    with open("sGhosts/sghost_{}.txt".format(level), 'r') as file:
                        sg_lines = file.readlines()
                        for i in range(len(sg_lines)):
                            sg_lines[i] = eval(sg_lines[i].strip())
                    with open("Ghosts/ghost_{}.txt".format(level), 'r') as file:
                        g_lines = file.readlines()
                        for i in range(len(g_lines)):
                            g_lines[i] = eval(g_lines[i].strip())
                    sghost_moves = []
                    ghost_moves = []
                    ghost_it = False
                    sghost_it = False
                    jon.hundred_percent = False
                    timer = 0
                    win_clock = 0
                    if settings[6]:
                        ghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color,
                                     ghost=True, show=g_show)
                        walls.append(ghost)
                    if settings[7]:
                        sghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color,
                                      ghost=True, show=sg_show)
                        walls.append(sghost)
                    for wall in walls[:-2]:
                        wall.rect.x += (WINDOWWIDTH - 800) // 2
                        wall.rect.y += (WINDOWHEIGHT - 600) // 2
                        wall.x += (WINDOWWIDTH - 800) // 2
                        wall.y += (WINDOWHEIGHT - 600) // 2
                    for coin in coins:
                        coin.rect.x += (WINDOWWIDTH - 800) // 2
                        coin.rect.y += (WINDOWHEIGHT - 600) // 2
                        coin.x += (WINDOWWIDTH - 800) // 2
                        coin.y += (WINDOWHEIGHT - 600) // 2
        for event in pygame.event.get():
            if event.type == pygame.VIDEORESIZE:
                x_speed -= (event.w - WINDOWWIDTH) / 2
                y_speed -= (event.h - WINDOWHEIGHT) / 2
                jon.rect.x += int(round((event.w - WINDOWWIDTH) / 2))
                jon.rect.y += int(round((event.h - WINDOWHEIGHT) / 2))
                WINDOWWIDTH = event.w
                WINDOWHEIGHT = event.h
                win = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.RESIZABLE)
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                exit()
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_PERIOD:
                    if sghost.show:
                        sghost.show = False
                    else:
                        sghost.show = True
                if event.key == pygame.K_COMMA:
                    if ghost.show:
                        ghost.show = False
                    else:
                        ghost.show = True
            if event.type == pygame.KEYDOWN and not win_level:
                if event.key == pygame.K_n:
                    if level < len(levels_list) - 1:
                        level += 1
                        gravity = 0
                        level_time = 0
                        win_clock = 0
                        timer = 0
                        teleport_timer = 0
                        g_frame = 0
                        with open("sGhosts/sghost_{}.txt".format(level), 'r') as file:
                            sg_lines = file.readlines()
                            for i in range(len(sg_lines)):
                                sg_lines[i] = eval(sg_lines[i].strip())
                        with open("Ghosts/ghost_{}.txt".format(level), 'r') as file:
                            g_lines = file.readlines()
                            for i in range(len(g_lines)):
                                g_lines[i] = eval(g_lines[i].strip())
                        sghost_moves = []
                        ghost_moves = []
                        ghost_it = False
                        sghost_it = False
                        jon.coin1 = False
                        jon.coin2 = False
                        jon.coin3 = False
                        paused = False
                        tot_x_mov = 0
                        tot_y_mov = 0
                        mini_map = []
                        levels_list = get_levels_list()
                        walls = levels_list[level][3:]
                        coins = levels_list[level][:3]
                        boud = find_bound(walls[1:])
                        gx = walls[0].rect.x
                        gy = walls[0].rect.y
                        for wall in walls[1:]:
                            mini_map.append(MiniWall(int((wall.rect.x - boud[0]) // 13) + 10,
                                                     int((wall.rect.y - WINDOWHEIGHT) // 10) + WINDOWHEIGHT,
                                                     int(wall.rect.width // 10), int(wall.rect.height // 10),
                                                     wall.color))

                        jon.hundred_percent = False
                        if settings[6]:
                            ghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color,
                                         ghost=True, show=g_show)
                            walls.append(ghost)
                        if settings[7]:
                            sghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color,
                                          ghost=True, show=sg_show)
                            walls.append(sghost)
                        for wall in walls[:-2]:
                            wall.rect.x += (WINDOWWIDTH - 800) // 2
                            wall.rect.y += (WINDOWHEIGHT - 600) // 2
                            wall.x += (WINDOWWIDTH - 800) // 2
                            wall.y += (WINDOWHEIGHT - 600) // 2
                        for coin in coins:
                            coin.rect.x += (WINDOWWIDTH - 800) // 2
                            coin.rect.y += (WINDOWHEIGHT - 600) // 2
                            coin.x += (WINDOWWIDTH - 800) // 2
                            coin.y += (WINDOWHEIGHT - 600) // 2
                if event.key == pygame.K_b:
                    if level > 0:
                        level -= 1
                        gravity = 0
                        teleport_timer = 0
                        g_frame = 0
                        with open("sGhosts/sghost_{}.txt".format(level), 'r') as file:
                            sg_lines = file.readlines()
                            for i in range(len(sg_lines)):
                                sg_lines[i] = eval(sg_lines[i].strip())
                        with open("Ghosts/ghost_{}.txt".format(level), 'r') as file:
                            g_lines = file.readlines()
                            for i in range(len(g_lines)):
                                g_lines[i] = eval(g_lines[i].strip())
                        sghost_moves = []
                        ghost_moves = []
                        ghost_it = False
                        sghost_it = False
                        jon.coin1 = False
                        jon.coin2 = False
                        jon.coin3 = False
                        paused = False
                        tot_x_mov = 0
                        tot_y_mov = 0
                        mini_map = []
                        levels_list = get_levels_list()
                        walls = levels_list[level][3:]
                        coins = levels_list[level][:3]
                        boud = find_bound(walls[1:])
                        gx = walls[0].rect.x
                        gy = walls[0].rect.y
                        for wall in walls[1:]:
                            mini_map.append(MiniWall(int((wall.rect.x - boud[0]) // 13) + 10,
                                                     int((wall.rect.y - WINDOWHEIGHT) // 10) + WINDOWHEIGHT,
                                                     int(wall.rect.width // 10), int(wall.rect.height // 10),
                                                     wall.color))

                        jon.hundred_percent = False
                        level_time = 0
                        win_clock = 0
                        timer = 0
                        if settings[6]:
                            ghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color,
                                         ghost=True, show=g_show)
                            walls.append(ghost)
                        if settings[7]:
                            sghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color,
                                          ghost=True, show=sg_show)
                            walls.append(sghost)
                        for wall in walls[:-2]:
                            wall.rect.x += (WINDOWWIDTH - 800) // 2
                            wall.rect.y += (WINDOWHEIGHT - 600) // 2
                            wall.x += (WINDOWWIDTH - 800) // 2
                            wall.y += (WINDOWHEIGHT - 600) // 2
                        for coin in coins:
                            coin.rect.x += (WINDOWWIDTH - 800) // 2
                            coin.rect.y += (WINDOWHEIGHT - 600) // 2
                            coin.x += (WINDOWWIDTH - 800) // 2
                            coin.y += (WINDOWHEIGHT - 600) // 2
        if keys[pygame.K_e]:
            dev()
        if keys[pygame.K_r] and not win_level:
            level = level
            gravity = 0
            jon.coin1 = False
            jon.coin2 = False
            jon.coin3 = False
            paused = False
            tot_x_mov = 0
            tot_y_mov = 0
            mini_map = []
            levels_list = get_levels_list()
            walls = levels_list[level][3:]
            coins = levels_list[level][:3]
            boud = find_bound(walls[1:])
            gx = walls[0].rect.x
            gy = walls[0].rect.y
            for wall in walls[1:]:
                mini_map.append(MiniWall(int((wall.rect.x - boud[0]) // 13) + 10,
                                         int((wall.rect.y - WINDOWHEIGHT) // 10) + WINDOWHEIGHT,
                                         int(wall.rect.width // 10), int(wall.rect.height // 10), wall.color))

            jon.hundred_percent = False
            teleport_timer = 0
            g_frame = 0
            if sg_show:
                with open("sGhosts/sghost_{}.txt".format(level), 'r') as file:
                    sg_lines = file.readlines()
                    for i in range(len(sg_lines)):
                        sg_lines[i] = eval(sg_lines[i].strip())
            if sg_show:
                with open("Ghosts/ghost_{}.txt".format(level), 'r') as file:
                    g_lines = file.readlines()
                    for i in range(len(g_lines)):
                        g_lines[i] = eval(g_lines[i].strip())
            sghost_moves = []
            ghost_moves = []
            ghost_it = False
            sghost_it = False
            fullrun_time = 0
            level_time = 0
            win_clock = 0
            timer = 0
            if settings[6]:
                ghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color, ghost=True,
                             show=g_show)
                walls.append(ghost)
            if settings[7]:
                sghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50, color, ghost=True,
                              show=sg_show)
                walls.append(sghost)
            for wall in walls[:-2]:
                wall.rect.x += (WINDOWWIDTH - 800) // 2
                wall.rect.y += (WINDOWHEIGHT - 600) // 2
                wall.x += (WINDOWWIDTH - 800) // 2
                wall.y += (WINDOWHEIGHT - 600) // 2
            for coin in coins:
                coin.rect.x += (WINDOWWIDTH - 800) // 2
                coin.rect.y += (WINDOWHEIGHT - 600) // 2
                coin.x += (WINDOWWIDTH - 800) // 2
                coin.y += (WINDOWHEIGHT - 600) // 2
        if not jon.hundred_percent:
            for coin in coins:
                if jon.rect.colliderect(coin.rect):
                    if coin.num == 1:
                        jon.coin1 = True
                    if coin.num == 2:
                        jon.coin2 = True
                    if coin.num == 3:
                        jon.coin3 = True
                    coins.remove(coin)
                    del coin
            if jon.coin1 and jon.coin2 and jon.coin3:
                jon.hundred_percent = True
                if coin_particles:
                    for i in range(5):
                        Particle(40, 40, GOLD)
                        Particle(80, 40, GOLD)
                        Particle(120, 40, GOLD)
        for wall in walls:
            if (
                    wall.rect.left == jon.rect.right or wall.rect.right == jon.rect.left) and jon.rect.bottom - jon.rect.height // 2 <= wall.rect.top <= jon.rect.bottom and wall.color != bg_color:
                if (int(x_speed // 1) > 0 and jon.rect.right <= wall.rect.left) or (int(x_speed // 1) < 0 and jon.rect.left >= wall.rect.right):
                    y_speed -= jon.rect.bottom - wall.rect.top
                    gravity = 0
            if (
                    (
                            wall.rect.left < jon.rect.left < wall.rect.right or wall.rect.left < jon.rect.right < wall.rect.right) or (
                            jon.rect.left < wall.rect.left < jon.rect.right or jon.rect.left < wall.rect.right < jon.rect.right)) and wall.color != bg_color:
                if jon.rect.bottom == wall.rect.top:
                    grounded = True
                    gravity = 0
                    x_speed += wall.move_sx
                    y_speed += wall.move_sy
                    if wall.color == YELLOW and keys[pygame.K_SPACE]:
                        walls.remove(wall)
                    if wall.color == PINK:
                        jump_factor = 2
                    if wall.color == BROWN:
                        jump_factor = 0.66
                    if wall.color == BLUE:
                        if bg_color == BLACK:
                            teleport_timer += 1
                        if teleport_timer == flip / 2:
                            teleport_timer = 0
                            x_speed += wall.tx
                            y_speed += wall.ty
                    if wall.color == GREEN:
                        jump_factor = 0
                    if wall.color == PURPLE:
                        paused = True
                    if wall.color == ORANGE:
                        paused = False
                    if wall.color == GOLD:
                        win_clock += 1
                        if win_clock == flip / 2:
                            win_level = True
                        if win_level:
                            if win_confetti:
                                Particle(jon.rect.x + jon.rect.width // 2, jon.rect.y + jon.rect.height // 2,
                                     (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255)))
                            keys = pygame.key.get_pressed()
                            with open(any_perc, 'r') as file:
                                lines = file.readlines()
                                level_record = float(lines[level])
                            run_time = level_time // 60 + ((level_time % 60) * (100 / 60)) / 100
                            best = '{:.2f}\n'.format(min(level_record, run_time))
                            lines[level] = best
                            if min(level_record, run_time) == run_time:
                                sghost_it = True
                                sum = 0
                                high_score = True
                                for i in range(len(lines) - 1):
                                    sum += float(lines[i])
                                lines[len(lines) - 1] = '{:.2f}'.format(sum)
                                with open(any_perc, 'w') as file:
                                    file.writelines(lines)
                            if jon.hundred_percent:
                                with open(full_finish, 'r') as file:
                                    lines = file.readlines()
                                    level_record = float(lines[level + 1])
                                best = '{:.2f}\n'.format(min(level_record, run_time))
                                lines[level + 1] = best
                                sum = 0
                                if min(level_record, run_time) == run_time:
                                    high_score = True
                                    ghost_it = True
                                    for i in range(1, len(lines) - 1):
                                        sum += float(lines[i])
                                    lines[len(lines) - 1] = '{:.2f}'.format(sum)
                                    with open(full_finish, 'w') as file:
                                        file.writelines(lines)
                            if keys[pygame.K_SPACE]:
                                print(run_time)
                                if level < len(levels_list) - 1:
                                    level += 1
                                gravity = 0
                                timer = 0
                                level_time = 0
                                win_clock = 0
                                teleport_timer = 0
                                g_frame = 0
                                if g_show:
                                    with open("Ghosts/ghost_{}.txt".format(level), 'r') as file:
                                        g_lines = file.readlines()
                                        for i in range(len(g_lines)):
                                            g_lines[i] = eval(g_lines[i].strip())
                                if sg_show:
                                    with open("sGhosts/sghost_{}.txt".format(level), 'r') as file:
                                        sg_lines = file.readlines()
                                        for i in range(len(sg_lines)):
                                            sg_lines[i] = eval(sg_lines[i].strip())
                                sghost_moves = []
                                ghost_moves = []
                                ghost_it = False
                                sghost_it = False
                                high_score = False
                                jon.coin1 = False
                                jon.coin2 = False
                                jon.coin3 = False
                                paused = False
                                tot_x_mov = 0
                                tot_y_mov = 0
                                if not jon.hundred_percent:
                                    fullrun_eligible = False
                                jon.hundred_percent = False
                                grounded = False
                                win_level = False
                                while len(Particle.particles) > 0:
                                    for part in Particle.particles:
                                        Particle.particles.remove(part)
                                        del part
                                levels_list = get_levels_list()
                                walls = levels_list[level][3:]
                                coins = levels_list[level][:3]
                                if settings[6]:
                                    ghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50,
                                                 color, ghost=True, show=g_show)
                                    walls.append(ghost)
                                if settings[7]:
                                    sghost = Wall(int(WINDOWWIDTH * 0.5 - 25), int(WINDOWHEIGHT * (2 / 3)), 50, 50,
                                                  color, ghost=True, show=sg_show)
                                    walls.append(sghost)
                                for wall in walls[:-2]:
                                    wall.rect.x += (WINDOWWIDTH - 800) // 2
                                    wall.rect.y += (WINDOWHEIGHT - 600) // 2
                                    wall.x += (WINDOWWIDTH - 800) // 2
                                    wall.y += (WINDOWHEIGHT - 600) // 2
                                for coin in coins:
                                    coin.rect.x += (WINDOWWIDTH - 800) // 2
                                    coin.rect.y += (WINDOWHEIGHT - 600) // 2
                                    coin.x += (WINDOWWIDTH - 800) // 2
                                    coin.y += (WINDOWHEIGHT - 600) // 2
                                mini_map = []
                                boud = find_bound(walls[1:])
                                gx = walls[0].rect.x
                                gy = walls[0].rect.y
                                for wall in walls[1:]:
                                    mini_map.append(MiniWall(int((wall.rect.x - boud[0]) // 13) + 10,
                                                             int((wall.rect.y - WINDOWHEIGHT) // 10) + WINDOWHEIGHT,
                                                             int(wall.rect.width // 10), int(wall.rect.height // 10),
                                                             wall.color))
                if jon.rect.top == wall.rect.bottom:
                    gravity = 0.1
        if keys[pygame.K_SPACE] and grounded:
            gravity -= g * 62.95 * jump_factor
        if pygame.mouse.get_pressed()[0]:
            for i in range(1):
                Particle(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1],
                         (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255)))
        for wall in walls:
            wall.draw(bg_color, win)
        if keys[pygame.K_TAB]:
            for wall in mini_map:
                wall.draw(bg_color, win)

        if fullrun_eligible:
            pygame.draw.circle(win, GREEN, (WINDOWWIDTH - 20, WINDOWHEIGHT - 20), 10)
        else:
            pygame.draw.circle(win, RED, (WINDOWWIDTH - 20, WINDOWHEIGHT - 20), 10)
        for coin in coins:
            coin.draw(win)
        if high_score:
            text = FONT.render('HIGH SCORE!', True, GREEN)
            win.blit(text, (
                WINDOWWIDTH // 2 - text.get_width() // 2, WINDOWHEIGHT // 3 - text.get_height() // 2))
        if win_level:
            text = font.render('Press spacebar for next level!', True, BLUE)
            win.blit(text, (
                WINDOWWIDTH // 2 - text.get_width() // 2, WINDOWHEIGHT // 2 - text.get_height() // 2))
        pygame.draw.circle(win, GREY, (40, 40), 19, 2)
        pygame.draw.circle(win, GREY, (80, 40), 19, 2)
        pygame.draw.circle(win, GREY, (120, 40), 19, 2)
        if jon.coin1:
            pygame.draw.circle(win, GOLD, (40, 40), 20)
            pygame.draw.circle(win, DGOLD, (40, 40), 20 - 20 // 3)
            pygame.draw.line(win, GOLD, (40 - 20 // 2, 40), (40 + 20 // 2, 40), 5)
        if jon.coin2:
            pygame.draw.circle(win, GOLD, (80, 40), 20)
            pygame.draw.circle(win, DGOLD, (80, 40), 20 - 20 // 3)
            pygame.draw.line(win, GOLD, (80 - 20 // 2, 40), (80 + 20 // 2, 40), 5)
        if jon.coin3:
            pygame.draw.circle(win, GOLD, (120, 40), 20)
            pygame.draw.circle(win, DGOLD, (120, 40), 20 - 20 // 3)
            pygame.draw.line(win, GOLD, (120 - 20 // 2, 40), (120 + 20 // 2, 40), 5)
        y_speed += gravity
        for particle in Particle.particles:
            particle.update()
            particle.y += 2
            particle.draw(win)

        if sghost_it and win_clock == flip / 2:
            with open("sGhosts/sghost_{}.txt".format(level), 'w') as file:
                for pair in ghost_moves:
                    file.write("{}\n".format(pair))
            sghost_it = False
        if ghost_it and win_clock == flip / 2:
            with open("Ghosts/ghost_{}.txt".format(level), 'w') as file:
                for pair in ghost_moves:
                    file.write("{}\n".format(pair))
            ghost_it = False
        if g_show:
            if not ghost_it and not sghost_it:
                if 0 < g_frame < len(sg_lines) - 1:
                    sghost.x += sg_lines[g_frame][0]
                    sghost.y += sg_lines[g_frame][1]
                if 0 < g_frame < len(g_lines) - 1:
                    ghost.x += g_lines[g_frame][0]
                    ghost.y += g_lines[g_frame][1]

        if not win_level:
            ghost_moves.append(f"[{-walls[0].rect.x + gx}, {-walls[0].rect.y + gy}]")
            gx = walls[0].rect.x
            gy = walls[0].rect.y
            g_frame += 1
            move(jon.rect, [-int(x_speed // 1), -int(y_speed // 1)], walls, coins, bg_color)
            x_speed -= x_speed // 1
            y_speed -= y_speed // 1
            tot_x_mov += -int(x_speed // 1)
            tot_y_mov += -int(y_speed // 1)

        pygame.draw.rect(win, GREY, (WINDOWWIDTH - 60, 10, 50, 10), 2)
        if bg_color == BLACK:
            pygame.draw.rect(win, WHITE, (WINDOWWIDTH - 60, 10, 50 * (timer % (flip*2)) // (flip*2), 10))
        if bg_color == WHITE:
            pygame.draw.rect(win, BLACK,
                             (WINDOWWIDTH - 10 - 50 * (timer % (flip*2)) // (flip*2), 10, 50 * (timer % (flip*2)) // (flip*2), 10))
        jon.draw(win)
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                exit()


if __name__ == "__main__":
    menu.menu()
